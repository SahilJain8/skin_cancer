import base64
from io import BytesIO

import numpy as np
from PIL import Image
from flask import Flask, jsonify, request
import tensorflow as tf
from tensorflow.keras.models import load_model

app = Flask(__name__)

model = load_model('model.h5') 


@app.route("/")
def index():
    return jsonify({"message":"hello"})








@app.route('/api/image', methods=['POST'])
def process_image():
    encoded_image = request.form.get('image')
    image_bytes = base64.b64decode(encoded_image)
    pil_image = Image.open(BytesIO(image_bytes))
    
    # Resize image to 100x100
    pil_image = pil_image.resize((100, 100))
    
    # Convert image to a 3D numpy array (100, 100, 3)
    img_array = np.array(pil_image)
   
    

    predictions = model.predict(np.expand_dims(img_array, axis=0))[0]
    predicted_class = np.argmax(predictions)
    
    response = {
        'message': 'Image processed successfully',
        'predicted_class': int(predicted_class)
    }
    
    return jsonify(response)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
